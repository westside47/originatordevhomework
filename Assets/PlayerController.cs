using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour
{
    private NavMeshAgent agent;
    private Animator animator;
    
    void Start() {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
    }
    
    void Update() {

        animator.SetBool("Movement",agent.hasPath);

        if (Input.GetMouseButtonDown(0) ) {

            //if over ui return the function
            if(EventSystem.current.IsPointerOverGameObject()){
                return;
            }

            //otherwise verify the click through and move the doggo
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100)) {
                agent.destination = hit.point;
            }
        }
    }

}
