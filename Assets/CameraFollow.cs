using UnityEngine;
using System.Collections;
public class CameraFollow : MonoBehaviour
{
    
    public GameObject player;
    private Vector3 offset;
    private Vector3 newtrans;

    void Start ()
    {
        offset = transform.position - player.transform.position;
        newtrans = transform.position;

    }
    void LateUpdate ()
    {
        newtrans = player.transform.position + offset;
        transform.position = newtrans;

    }


}